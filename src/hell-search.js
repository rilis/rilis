'use strict';
/* globals XMLHttpRequest, document, XDomainRequest */
/* exported trySearch */

var timeout;
var actions = Promise.resolve();

function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        xhr.open(method, url, true);
    } else if (typeof XDomainRequest != "undefined") {
        xhr = new XDomainRequest();
        xhr.open(method, url);
    } else {
        xhr = null;
    }
    return xhr;
}

function promiseSendSearchRequest(query) {
    return new Promise(function(resolve, reject) {
        var xhr = createCORSRequest("POST", "https://hell.rilis.io/search");
        if (!xhr) {
            reject();
        } else {
            xhr.onerror = function() {
                reject();
            };
            xhr.onload = function() {
                resolve(xhr.responseText);
            };

            xhr.send(JSON.stringify({
                request: query
            }));
        }
    }).then(function(data) {
        return JSON.parse(data);
    });
}

function renderResultFromTemplate(name, url, popularity, topics, description) {
    return "<h2><a target=\"_blank\" href=\"" + url + "\">" + name + "</a></h2><ul><li>" + description + "</li><li>Popularity: " + popularity + "</li><li>Tags: " + topics + "</li></ul>";
}

function renderResults(response) {
    if (response.length === 0) {
        return "<br><b>Unfortunately no awesome projects found for your query. Please use different words to describe these impressive projects you are looking for.</b>";
    } else {
        var results = "<h1> </h1>";
        response.forEach(function(project) {
            results += renderResultFromTemplate(project.name,
                project.url,
                project.popularity,
                project.topics.map(function(e) {
                    return "<code>" + e + "</code>";
                }).join(", "),
                project.description);
        });
        return results;
    }
}

function search() {
    var request = document.getElementById("query").value;
    if (request.length > 0) {
        actions.then(function() {
            return promiseSendSearchRequest(request);
        }).then(function(response) {
            document.getElementById("results").innerHTML = renderResults(response);
        }, function() {
            document.getElementById("results").innerHTML = "<b>Searching not works! Please create <a href=\"https://gitlab.com/rilis/website/issues\">issue</a>.</b>";
        });
    }
}

function trySearch() {
    if (timeout !== undefined) {
        clearTimeout(timeout);
    }
    timeout = setTimeout(function() {
        search();
    }, 1000);
}