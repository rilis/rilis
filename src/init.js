"use strict";
/* globals $, document, window */
$(function() {
    var selected_first_time = true;
    var links = document.links;
    for (var i = 0; i < links.length; i++) {
        if (links[i].hostname !== window.location.hostname) {
            links[i].target = '_blank';
        }
    }

    $.jstree.plugins.nohover = function() {
        this.hover_node = $.noop;
    };

    $('#menu').jstree({
        core: {
            themes: {
                dots: false,
                icons: true,
                ellipsis: false,
                stripes: false,
                responsive: false,
                variant: 'small'
            }
        },
        state: {
            key: 'rilis-menu-state',
            ttl: 60000
        },
        plugins: ['state', 'nohover']
    });

    $('#menu').on("changed.jstree", function(e, data) {
        if (data.action === 'select_node') {
            if (selected_first_time) {
                selected_first_time = false;
            } else if (window.location.href !== data.node.a_attr.href) {
                window.location.href = data.node.a_attr.href;
            }
        }
    });
    $('#menu').on('loaded.jstree', function(e, data) {
        var depth = 3;
        data.inst.get_container().find('li').each(function(i) {
            if (data.inst.get_path($(this)).length <= depth) {
                data.inst.open_node($(this));
            }
        });
    });
});