'use strict';
var fs = require("fs");
var path = require("path");
var yaml = require("js-yaml");
var hljs = require('highlight.js');
var md = require('markdown-it')({
        html: true,
        linkify: false,
        typographer: false,
        breaks: false,
        highlight: function(str, lang) {
            if (lang && hljs.getLanguage(lang)) {
                try {
                    var r = hljs.highlight(lang, str, true);
                    return r.value;
                } catch (__) {}
            }
            return '';
        }
    })
    .use(require('markdown-it-anchor'), {
        level: 2,
        permalink: true,
        permalinkClass: 'md-header-anchor',
        permalinkSymbol: '#',
        permalinkBefore: true
    })
    .use(require('markdown-it-imsize'))
    .use(require('markdown-it-fontawesome'))
    .use(require('markdown-it-plantuml'))
    .use(require('markdown-it-container'), 'note', {
        render: function(tokens, idx) {
            if (tokens[idx].nesting === 1) {
                return '<div class="md-note md-info-note"><b>Note: </b>';
            } else {
                return "</div>";
            }
        }
    })
    .use(require('markdown-it-container'), 'warning', {
        render: function(tokens, idx) {
            if (tokens[idx].nesting === 1) {
                return '<div class="md-note md-warning-note"><b>Warning: </b>';
            } else {
                return "</div>";
            }
        }
    })
    .use(require('markdown-it-checkbox'), {
        disabled: true,
        divWrap: false,
        divClass: 'checkbox',
        idPrefix: 'cbx_',
        ulClass: 'task-list',
        liClass: 'task-list-item'
    });

function createHtmlFromTemplate(title, menu, content, relative, current) {
    return `<!doctype html>
<html lang='en'>
<head>
<title>Rili's::${title}</title>
<meta charset='utf-8'>
<meta name='viewport' content='width=device-width, initial-scale=1'>
<meta name='theme-color' content='#53b6ff'>
<link rel='icon' type='image/png' href='${relative}static/icon-64x64.png'>
<link rel='manifest' href='${relative}static/manifest.json'>
<link rel='stylesheet' type='text/css' href='${relative}static/style-min.css'/>
<link rel='stylesheet' type='text/css' href='${relative}static/css/font-awesome.min.css'/>
<script>
function onLoad(){var l=document.links;for(var i=0;i<l.length;i++){if(l[i].hostname!==window.location.hostname){l[i].target='_blank';}}}
</script>
</head>
<body onload="onLoad();">
<div id='left'>
<div id = 'logo'>
<a href='${relative}'>
<img id='logo-image' src='${relative}static/logo.png' alt="Rili's"/>
</a>
</div>
<div id='menu'>
${menu}
</div>
</div>
<div id='content'>
${content}
</div>
</body>
</html>
`;
}

function globSyncHelper(absolute, root, relative) {
    var files = fs.readdirSync(absolute);
    files.forEach(function(file) {
        var joined = path.join(absolute, file);
        var child;
        if (fs.statSync(joined).isDirectory()) {
            child = {
                path: path.relative(relative, joined),
                child: [],
                parent: root
            };
            globSyncHelper(joined, child, relative);
        } else {
            if (joined.endsWith(".yml")) {
                var newPath = joined.substr(0, joined.length - 4);
                if (path.basename(newPath) === "index") {
                    newPath += ".html";
                } else {
                    newPath = path.join(newPath, "index.html");
                }
                child = {
                    path: path.relative(relative, newPath),
                    document: yaml.safeLoad(fs.readFileSync(joined, "utf-8")),
                    parent: root
                };
            }
        }
        root.child.push(child);
        root.child = root.child.sort(function(a, b) {
            return a.path > b.path ? 1 : -1;
        });
    });
}

function loadDocumentsTree(dir) {
    var absolute = dir;
    if (!path.isAbsolute(dir)) {
        absolute = path.join(process.cwd(), dir);
    }

    var root = {
        path: "",
        child: [],
        parent: null
    };

    globSyncHelper(absolute, root, dir);
    return root;
}


function walkTree(tree, onDocument, onDirectoryIn, onDirectoryOut) {
    if (tree.document !== undefined && onDocument) {
        onDocument(tree);
    } else if (tree.child !== undefined) {
        if (onDirectoryIn) {
            onDirectoryIn(tree);
        }
        tree.child.forEach(function(child) {
            walkTree(child, onDocument, onDirectoryIn, onDirectoryOut);
        });
        if (onDirectoryOut) {
            onDirectoryOut(tree);
        }
    }
}

function walkTreeDocuments(tree, onDocument) {
    walkTree(tree, onDocument);
}

function walkTreeDirectories(tree, onDirectory) {
    walkTree(tree, undefined, onDirectory);
}

function removeEmptyDirectories(tree) {
    var modified;
    var checker = function(entry) {
        if (entry.child.length === 0) {
            modified = true;
            entry.parent.child = entry.parent.child.filter(function(ch) {
                return ch !== entry;
            });
        }
    };
    do {
        modified = false;
        walkTreeDirectories(tree, checker);
    } while (modified);
}

function renderMdDocumentContents(tree) {
    walkTreeDocuments(tree, function(entry) {
        if (entry.document.type === "markdown") {
            entry.document.content = "<div class='markdown-body'>" + md.render("# " + entry.document.title + "\n" + entry.document.content) + "</div>";
        }
    });
}

function renderMenus(tree) {
    walkTreeDocuments(tree, function(entry) {
        entry.menu = renderMenu(tree, entry);
    });
}

function ensureDirSync(targetDir) {
    targetDir.split(path.sep).forEach(function(dir, index, splits) {
        var dirPath = splits.slice(0, index + 1).join(path.sep);
        if (index > 0) {
            if (!fs.existsSync(dirPath)) {
                fs.mkdirSync(dirPath);
            }
        }
    });
}

function saveTree(tree, destinationRootDirectory) {
    walkTreeDocuments(tree, function(entry) {
        var relative = calculateRelative(entry);
        var html = createHtmlFromTemplate(entry.document.title, entry.menu, entry.document.content, relative, entry.path);
        var destination = path.join(destinationRootDirectory, entry.path.toLowerCase());
        ensureDirSync(path.dirname(destination));
        fs.writeFileSync(destination, html);
    });
}

function renderMenuEntryFromTemplate(withLink, icon, href, bbegin, name, bend) {
    if (withLink) {
        return `<li><i class='${icon}' aria-hidden='true'></i> <a href='${href}'>${bbegin}${name}${bend}</a>`;
    } else {
        return `<li><i class='${icon}' aria-hidden='true'></i> ${bbegin}${name}${bend}`;
    }
}

function renderMenu(root, current) {
    var relative = calculateRelative(current);
    var result = "<ul class=\'lmenu\'>";
    var onDocument = function(entry) {
        if (entry.path !== path.join(entry.parent.path, "index.html")) {
            var location = ".";
            if (entry === current) {
                result += renderMenuEntryFromTemplate(true, entry.icon, location.toLowerCase(), "<b>", entry.document.title, "</b>") + "</li>";
            } else {
                var from = path.join(relative, path.dirname(current.path));
                var to = path.join(relative, path.dirname(entry.path));
                location = path.relative(from, to) + "/";
                result += renderMenuEntryFromTemplate(true, entry.icon, location.toLowerCase(), "", entry.document.title, "") + "</li>";
            }
        }
    };

    var haveIndex = function(entry) {
        return entry.child.filter(function(c) {
            return c.path === path.join(entry.path, "index.html");
        }).length > 0;
    };
    var onDirIn = function(entry) {
        if (entry !== root) {
            var location = ".";
            if (current.path === path.join(entry.path, "index.html")) {
                result += renderMenuEntryFromTemplate(haveIndex(entry), entry.icon, location.toLowerCase(), "<b>", path.basename(entry.path), "</b>");
            } else {
                var from = path.join(relative, path.dirname(current.path));
                var to = path.join(relative, entry.path);
                location = path.relative(from, to) + "/";
                result += renderMenuEntryFromTemplate(haveIndex(entry), entry.icon, location.toLowerCase(), "", path.basename(entry.path), "");
            }
            result += "<ul class=\'lmenu\'>";
        }
    };
    var onDirOut = function(entry) {
        if (entry !== root) {
            result += "</ul></li>";
        }
    };
    walkTree(root, onDocument, onDirIn, onDirOut);
    return result + "</ul>";
}

function addDefaultIcons(tree) {
    walkTreeDocuments(tree, function(entry) {
        if (entry.icon === undefined) {
            if (entry.document.icon === undefined) {
                if (entry.path === path.join(entry.parent.path, "index.html")) {
                    entry.document.icon = "fa fa-folder-open-o";
                } else {
                    entry.document.icon = "fa fa-file-o";
                }
            }
            entry.icon = entry.document.icon;
        }
        if (entry.path === path.join(entry.parent.path, "index.html")) {
            entry.parent.icon = entry.icon;
        }
    });

    walkTreeDirectories(tree, function(entry) {
        if (entry.icon === undefined) {
            entry.icon = "fa fa-folder-open-o";
        }
    });
}

function removeDrafts(tree) {
    walkTreeDocuments(tree, function(entry) {
        if (entry.document.draft === true) {
            entry.parent.child = entry.parent.child.filter(function(c) {
                return c !== entry;
            });
        }
    });
}

function calculateRelative(entry) {
    var relative = entry.path.split(path.sep);
    relative = relative.slice(0, relative.length - 1).map(function() {
        return "..";
    }).join(path.sep);
    return relative.length > 0 ? relative + path.sep : "";
}

module.exports = function(args) {
    var documentTree = loadDocumentsTree(args[0]);

    removeDrafts(documentTree);
    removeEmptyDirectories(documentTree);
    addDefaultIcons(documentTree);
    renderMdDocumentContents(documentTree);
    renderMenus(documentTree);
    saveTree(documentTree, args[1]);
};