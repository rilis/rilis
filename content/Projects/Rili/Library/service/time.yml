type: markdown
title: Time
icon: fa fa-file-code-o
content: |
    ## API reference
     * [`rili::service::Time`](https://rilis.io/doxygen/classrili_1_1service_1_1_time.html)
     * [`rili::service::TimeBase`](https://rilis.io/doxygen/classrili_1_1service_1_1_time_base.html)

    ## Examples
    Very simple rili::service::time usage
    ```cpp
    #include <chrono>
    #include <iostream>
    #include <random>
    #include <rili/Promise.hpp>
    #include <rili/service/Time.hpp>

    int main(int, char**) {
        std::cout << "synchronous start" << std::endl;

        rili::Context::run([]() {
            std::cout << "async start" << std::endl;

            rili::Promise<int>([](rili::Promise<int>::OnResolveHandler resolve) {
                std::cout << "hello ";
                std::random_device rd;
                resolve(rd() % 10);
            }).Then([](int v) {
                return rili::service::Time::get().promiseFor(std::chrono::seconds(v)).Then([]() {
                    std::cout << "word!" << std::endl;
                });
            });

            std::cout << "async end" << std::endl;
        });

        std::cout << "synchronous end" << std::endl;
        return 0;
    }
    ```

    This time with `promiseUntil` instead of `promiseFor`
    ```cpp
    #include <iostream>
    #include <rili/Context.hpp>
    #include <rili/service/Time.hpp>

    int main(int /*argc*/, char** /*argv*/) {
        std::cout << "Hello!" << std::endl;
        rili::Context::run([]() {
            std::cout << "Now we are running in rili::Context. Starting timers!" << std::endl;
            auto& service = rili::service::Time::get();

            service.promiseUntil(rili::service::Time::Clock::now() + std::chrono::seconds(5)).Then([]() {
                std::cout << "We are 5 seconds later" << std::endl;
            });
            service.promiseFor(std::chrono::seconds(10)).Then([]() { std::cout << "Now even 10." << std::endl; });

            std::cout << "Timers started. Initializer will finish soon..." << std::endl;
        });

        std::cout << "That's all ;)" << std::endl;
        return 0;
    }
    ```

    More complex example showing usage of `rili::CancelablePromise` and timers.
    ```cpp
    #include <chrono>
    #include <iostream>
    #include <rili/CancelablePromise.hpp>
    #include <rili/service/Time.hpp>

    int main(int, char**) {
        std::cout << "Narrator: Short story about fight between bright and dark side of power." << std::endl;
        rili::Context::run([]() {
            auto happyPromise =
                rili::CancelablePromise<void>([](rili::CancelablePromise<void>::OnResolveHandler const& /*resolve*/,
                                                 rili::CancelablePromise<void>::OnRejectHandler const& /*reject*/,
                                                 rili::CancelablePromise<void>::OnCancel const& onCancel) {
                    std::cout << "Happy Promise: World is great and I want live forever..." << std::endl;
                    onCancel([]() { std::cout << "Happy Promise: Outh! That hurts! I don't want die..." << std::endl; });
                });
            happyPromise.Finally([]() { std::cout << "Narrator: Happy Promise died :(" << std::endl; });

            rili::service::Time::get()
                .promiseFor(std::chrono::seconds(15))
                .Then([]() -> rili::Promise<void> {
                    std::cout << "Evil Promise: Ha! I'm Evil Promise. I don't like other promises. I will kill them!!!"
                              << std::endl;
                    return rili::service::Time::get().promiseFor(std::chrono::seconds(15));
                })
                .Then([happyPromise]() mutable {
                    std::cout << "Evil Promise: Happy Promise, your death is coming!!!" << std::endl;
                    happyPromise.cancel();
                })
                .Finally([]() {
                    std::cout << "Evil Promise: My life mission to kill all other promises is done. Now I will make sepuku."
                              << std::endl;
                });
        });
        std::cout << "Narrator: Bravo!!!" << std::endl;
        return 0;
    }
    ```
